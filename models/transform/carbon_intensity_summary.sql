WITH entry AS (

     SELECT *
     FROM {{ref('carbon_intensity_entry')}}

),

generationmix AS (

     SELECT *
     FROM {{ref('carbon_intensity_generationmix')}}

),

region AS (

     SELECT *
     FROM {{ref('carbon_intensity_region')}}

)

SELECT
    e.forecast,
    e."from",
    e."to",
    g.fuel,
    g.perc,
    r.shortname
FROM entry e
  JOIN generationmix g  ON e.id = g.entry_id
  JOIN region r           ON e.region_id = r.id