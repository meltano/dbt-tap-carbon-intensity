# dbt | tap-carbon-intensity

This [dbt](https://github.com/fishtown-analytics/dbt) package contains data models for [tap-carbon-intensity](https://gitlab.com/meltano/tap-carbon-intensity).
